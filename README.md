Nordseebojen-Quellcode
--
Dieser Code ist für einen ESP 32 Mikrocontroler für die Arduino IDE geschrieben. 
Dabei geht es darum verschiedene Messwerte, wie z.B. Salzgehalt, Temperatur, einer Nordseeboje zu messen und in Echtzeit über das LoraWAN Netwerk zu verschicken. Das interessante an dieser Boje, die an der HTS Husum entwickelt wird, ist eine Überwachung von Muschelbänken.
Dabei sind werden bis zu 10 Muscheln in einem Metallkäfig fixiert und über Magnetsensoren beobachtet, wann diese sich im Meer öffnen bzw. schließen. Diese Daten werden gesammelt und können Live eingesehen werden unter
http://www.the-centaurus.com/bojen

<p align="center">
  <img src="muscheln.png" width="300" title="Bild des Muschelmonitors">
</p>
Bild des Muschelmonitors


Bibliotheken
--
Das Hauptfile ist der .ino-Sketch.
Die beiden zusätzlichen Ordner enthalten notwendige Bibliotheken. Diese müssen einzeln importiert werden unter Sketch > Bibliothek einbinden > .ZIP Bibliothek hinzufügen. <br>

Board-Einbinden
--
Um den Code für den ESP 32 zu kompilieren, muss folgender Link eingebunden werden. 
Klicken sie dazu auf Datei->Voreinstellungen und fügen Sie dann unter URLS folgende URL hinzu:

https://dl.espressif.com/dl/package_esp32_index.json


Starten Sie dann das Programm neu. Dann klicke auf Werkzeuge->Boardverwalter, suche nach esp und installiere das entsprechende Packet. Klicken sie unter Tools->Board und wählen Sie  TTGO LoRa32-OLED V1 aus.
Nun müsste alles bereit sein zum kompilieren und übertragen. Bei Fragen gerne melden.

Ports
---
Folgendende Ports werden angesprochen:
<p align="center">
  <img src="Ports.png" width="550" title="Port Selection">
</p>

Viel Erfolg,
Thore
