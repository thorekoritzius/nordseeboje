  /*
      Nordsee Messboje, Version 1.2
      Code für TTGO LoRa32-OLED V1 auf dem ESP32
      Thore Koritzius, June 2020

      Ports

      References:
      https://learn.sparkfun.com/tutorials/esp32-lora-1-ch-gateway-lorawan-and-the-things-network/all
      https://www.thethingsnetwork.org/forum/t/esp32-node-not-sending/33176
*/
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include "DateTime.h"

#include "FS.h"
#include "SD_MMC.h"


#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  900      /* Time ESP32 will go to sleep (in seconds) */

RTC_SLOW_ATTR  long bootCount = 0;
RTC_SLOW_ATTR boolean SDCard = true;

String beschriftung = "A1;A2;A3;A4;A5;D1;D2;D3;D4;D5;D6;D7;D8;D9;D10;SD-Card;Number;Boje-Nr;";


// Below sets the trigger for sending a new message to a gateway.
// Either or both can be enabled (in which case pressing the button will restart the timer)
#define SEND_BY_BUTTON 1  // Send a message when button "0" is pressed
#define SEND_BY_TIMER 1 // Send a message every TX_INTERVAL seconds

  
//analog pins
const int AP1 = 36;
const int AP2 = 39;
const int AP3 = 34;
const int AP4 = 35;
const int AP5 = 26;

//digital pins
const int DP1 = 14;
const int DP2 = 12;
const int DP3 = 13;
const int DP4 = 15;
const int DP5 = 2;

const int DP6 = 25;
const int DP7 = 21;
const int DP8 = 22;
const int DP9 = 23;
const int DP10 = 19;
char compile_time[] =  __TIME__;
char compile_date[] =  __DATE__;



RTC_DATA_ATTR int StartSec = 0;
RTC_DATA_ATTR int StartMin = 0;
RTC_DATA_ATTR int StartHour = 0;

RTC_DATA_ATTR int StartDay = 0;
RTC_DATA_ATTR int StartMonth = 0;
RTC_DATA_ATTR int StartYear = 0;

String bojeNr = "Boje_0";
RTC_SLOW_ATTR String fileName = "/" + bojeNr + " data.txt";
// LoRaWAN NwkSKey, network session key
// This is the default Semtech key, which is used by the early prototype TTN
// network.
static const PROGMEM u1_t NWKSKEY[16] = { TODO };
// LoRaWAN AppSKey, application session key
// This is the default Semtech key, which is used by the early prototype TTN
// network.
static const u1_t PROGMEM APPSKEY[16] = { TODO };

// LoRaWAN end-device address (DevAddr)
static const u4_t DEVADDR = TODO;

// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

static uint8_t mydata[] = "HI";
static osjob_t sendjob;

String res = "";

// Pin mapping
const lmic_pinmap lmic_pins = {
  .nss = 18,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 14,
  .dio = {26, 32, 33},
};

// If send-by-timer is enabled, define a tx interval
#ifdef SEND_BY_TIMER
#define TX_INTERVAL 60 // Message send interval in seconds
#endif

uint32_t kounter;

void setup() {
  Serial.begin(115200);

  if(bootCount == 0){
    init_clock(true);
    String t = getTimeString();
    t.replace(":", "");
    t.replace(".", "");
  }
  else
    init_clock(false);

  ++bootCount;
  Serial.println("BootCount " + String(bootCount));
  

  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);

#if defined(CFG_eu868) // EU channel setup
  // Set up the channel used by the Things Network and compatible with
  // our gateway.
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set.
  LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
  LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
  LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
  LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
  LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
  LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
  LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);      // g-band
  LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK, DR_FSK), BAND_MILLI);      // g2-band


#endif
  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;

  LMIC_setDrTxpow(DR_SF7, 14);
  measurement();
  boolean SDPresent = true;
  if(!writeToSD(getMeasurement()))
    SDPresent = writeToSD(getMeasurement());

  
  if(!SDPresent)
    SDCard = false;
  else
    SDCard = true;
    
  do_send(&sendjob);

  delay(1000);


  os_runloop_once();
  
  //delay(10000);
  Serial.println("Initiate " + (String) TIME_TO_SLEEP + "s Sleep");

  if(DateTime.available()) { // update memory
    StartSec = DateTime.Second;
    StartMin = DateTime.Minute;
    StartHour = DateTime.Hour;

    StartYear = ((DateTime.Year) + 1900);
    StartMonth= (DateTime.Month);
    StartDay  = DateTime.Day;
  }
  
  esp_sleep_pd_config(ESP_PD_DOMAIN_MAX, ESP_PD_OPTION_OFF);
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);


  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  esp_deep_sleep_start();
}
String getMeasurement(){
  measurement();
  return res;
}

void loop() {

}

boolean writeToSD(String input){
   if(!SD_MMC.begin()){
        return false;
    }
    uint8_t cardType = SD_MMC.cardType();

    if(cardType == CARD_NONE){
        Serial.println("No SD_MMC card attached");
        SDCard = false;
        return false;
    }

    Serial.print("SD_MMC Card Type: ");
    if(cardType == CARD_MMC){
        Serial.println("MMC");
    } else if(cardType == CARD_SD){
        Serial.println("SDSC");
    } else if(cardType == CARD_SDHC){
        Serial.println("SDHC");
    } else {
        Serial.println("UNKNOWN");
    }

    uint64_t cardSize = SD_MMC.cardSize() / (1024 * 1024);
    Serial.printf("SD_MMC Card Size: %lluMB\n", cardSize);


    input.replace(";;", ";");
    input.replace("                   ;", "");

    //writeTo SD
    char *fN = new char[fileName.length()];
    strcpy(fN, fileName.c_str());
    if(!readFile(SD_MMC, fN)){
        char *msg = new char[(beschriftung + "\n"+ input + "\n").length()];
        strcpy(msg, (beschriftung + "\n"+ input + "\n").c_str());
        writeFile(SD_MMC,fN , msg);
    }else{
        char *msg = new char[(input + "\n").length()];
        strcpy(msg, (input + "\n").c_str());
        appendFile(SD_MMC, fN, msg);
    }
    return true;
}

void init_clock(bool first){
  
  if(first){
      int sec = ( compile_time[6]-'0') * 10 + (compile_time[7]-'0' ) + 10;
      int h = (compile_time[0]-'0' ) * 10 + (compile_time[1]-'0');
      int m = ( compile_time[3]-'0') * 10 + (compile_time[4]-'0' );
      int d = ( compile_date[4]-'0') * 10 + (compile_date[5]-'0' );
      int y = ( compile_date[7]-'0') * 1000 + (compile_date[8]-'0' )*100 + (compile_date[9]-'0' )*10 + (compile_date[10]-'0' );
      int mo = convertMonth(String( compile_date[0])+String( compile_date[1])+String( compile_date[2]));
      DateTime.sync(0);
      //DateTime.sync(DateTime.makeTime(sec,m,h,d,mo,y));
      DateTime.sync(DateTime.makeTime(0,0,14,1,7,2020));
  }
  else{
    StartMin+= 15;
    DateTime.sync(DateTime.makeTime(StartSec,StartMin,  StartHour, StartDay, StartMonth, StartYear ));
  }
  
  
}

String measurement(){
  String timeString=getTimeString();

  pinMode(AP1, INPUT);
  pinMode(AP2, INPUT);
  pinMode(AP3, INPUT);
  pinMode(AP4, INPUT);
  pinMode(AP5, INPUT);

  
  pinMode(DP1, INPUT);
  pinMode(DP2, INPUT);
  pinMode(DP3, INPUT);
  pinMode(DP4, INPUT);
  pinMode(DP5, INPUT);
  pinMode(DP6, INPUT);
  pinMode(DP7, INPUT);
  pinMode(DP8, INPUT);
  pinMode(DP9, INPUT);
  pinMode(DP10, INPUT);
  
  //Read all Sensordata Analog:
  int SA1 = analogRead(AP1);
  int SA2 = analogRead(AP2);
  int SA3 = analogRead(AP3);
  int SA4 = analogRead(AP4);
  int SA5 = analogRead(AP5);

  
  //Read all Sensordata Digital:
  int SD1 = digitalRead(DP1);
  int SD2 = digitalRead(DP2);
  int SD3 = digitalRead(DP3);
  int SD4 = digitalRead(DP4);
  int SD5 = digitalRead(DP5);
  int SD6 = digitalRead(DP6);
  int SD7 = digitalRead(DP7);
  int SD8 = digitalRead(DP8);
  int SD9 = digitalRead(DP9);
  int SD10 = digitalRead(DP10);

  String alterNate = "                   ";
  String package = alterNate+ ";" +print4Digits(SA1) + ";" + print4Digits(SA2) + ";" + print4Digits(SA3) + ";" + print4Digits(SA4) + ";" + print4Digits(SA5) + ";"
  +(String)SD1+ ";"+(String)SD2+ ";"+(String)SD3+ ";"+(String)SD4+ ";"+(String)SD5+ ";"
  +(String)SD6+ ";"+(String)SD7+ ";"+(String)SD8+ ";"+(String)SD9+ ";"+(String)SD10+";"+ (String)SDCard + ";;"+print6Digits(bootCount)  + ";"+ bojeNr + ";";
  if(res.equals(""))
    res = package;
  return package;
}
String getTimeString(){
  if(DateTime.available()) { // update clock
    unsigned long prevtime = DateTime.now();
    while(prevtime == DateTime.now());       
    DateTime.available();
    String timeString = printDigits(DateTime.Hour) + ":"+printDigits(DateTime.Minute)+ ":" +printDigits(DateTime.Second) + " " +  
    printDigits(DateTime.Day) + "." + printDigits((int)(DateTime.Month))+ "." +(String)((DateTime.Year) + 1900);

    StartSec = DateTime.Second;
    StartMin = DateTime.Minute;
    StartHour = DateTime.Hour;

    StartYear = ((DateTime.Year) + 1900);
    StartMonth= (DateTime.Month);
    StartDay  = DateTime.Day;
    return timeString;
  }
  return "";
}

// State machine event handler
void onEvent (ev_t ev) {
  Serial.print(os_getTime());
  Serial.print(": ");
  switch (ev) {
    case EV_SCAN_TIMEOUT:
      Serial.println(F("EV_SCAN_TIMEOUT"));
      break;
    case EV_BEACON_FOUND:
      Serial.println(F("EV_BEACON_FOUND"));
      break;
    case EV_BEACON_MISSED:
      Serial.println(F("EV_BEACON_MISSED"));
      break;
    case EV_BEACON_TRACKED:
      Serial.println(F("EV_BEACON_TRACKED"));
      break;
    case EV_JOINING:
      Serial.println(F("EV_JOINING"));
      break;
    case EV_JOINED:
      Serial.println(F("EV_JOINED"));
      break;
    case EV_RFU1:
      Serial.println(F("EV_RFU1"));
      break;
    case EV_JOIN_FAILED:
      Serial.println(F("EV_JOIN_FAILED"));
      break;
    case EV_REJOIN_FAILED:
      Serial.println(F("EV_REJOIN_FAILED"));
      break;
    case EV_TXCOMPLETE:
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      if (LMIC.txrxFlags & TXRX_ACK)
        Serial.println(F("Received ack"));
      if (LMIC.dataLen) {
        Serial.println(F("Received "));
        Serial.println(LMIC.dataLen);
        Serial.println(F(" bytes of payload"));
      }
#ifdef SEND_BY_TIMER
      // Schedule the next transmission
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
#endif
      break;
    case EV_LOST_TSYNC:
      Serial.println(F("EV_LOST_TSYNC"));
      break;
    case EV_RESET:
      Serial.println(F("EV_RESET"));
      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
      Serial.println(F("EV_RXCOMPLETE"));
      break;
    case EV_LINK_DEAD:
      Serial.println(F("EV_LINK_DEAD"));
      break;
    case EV_LINK_ALIVE:
      Serial.println(F("EV_LINK_ALIVE"));
      break;
    default:
      Serial.println(F("Unknown event"));
      break;
  }
}

// Transmit data from mydata
void do_send(osjob_t* j) {
  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND) {
    Serial.println(F("OP_TXRXPEND, not sending"));
  } else {
    String m = res;
    m.replace("0;;", (String)SDCard + ";");
    m.replace("1;;", (String)SDCard + ";");
    byte plain[m.length()];
    m.getBytes(plain, m.length());
    Serial.println(m);
    LMIC_setTxData2(1, plain, sizeof(plain) - 1, 0);
    Serial.println(F("Packet queued"));
  }
}

int convertMonth(String m){
  String mo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sept", "Oct", "Nov", "Dec"};
  for(int i = 0; i < 12; i++){
    if(mo[i].equals(m))
      return i+1;
  }
  return 0;
}
String printDigits(int digits){
  String res = "";
  if(digits < 10)
    res+=('0');
  res+=(String)digits;
  return res;
}
String print4Digits(int digits){
  String res = "";
  if(digits < 1000)
    res+=('0');
  if(digits < 100)
    res+=('0');
  if(digits < 10)
    res+=('0');
  res+=(String)digits;
  return res;
}
String print6Digits(long digits){
  String res = "";
  if(digits < 100000)
    res+=('0');
  if(digits < 1000)
    res+=('0');
  if(digits < 1000)
    res+=('0');
  if(digits < 100)
    res+=('0');
  if(digits < 10)
    res+=('0');
  res+=(String)digits;
  return res;
}
boolean readFile(fs::FS &fs, const char * path){
    File file = fs.open(path);
    if(!file){
      return false;
    }
    return true;
}

void writeFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        SDCard = false;
        //return;
    }
    if(file.print(message)){
        Serial.println("File written");
        SDCard = true;
    } else {
        Serial.println("Write failed");
        SDCard = false;
    }
}

void appendFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        Serial.println("Failed to open file for appending");
        SDCard = false;
      //  return;
    }
    if(file.print(message)){
        Serial.println("Message appended");
        SDCard = true;
    } else {
        Serial.println("Append failed");
        SDCard = false;
    }
}